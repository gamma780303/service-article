import Article from "./article";
import {
    Column,
    PrimaryKey,
    Model,
    DataType,
    Table,
    ForeignKey,
    AllowNull,
    BelongsTo, HasMany
} from "sequelize-typescript";
import Report from "./report";

@Table({tableName: 'comments'})
class Comment extends Model<Partial<Comment>> {
    @PrimaryKey
    @Column({
        type: DataType.UUIDV4,
        defaultValue: DataType.UUIDV4
    })
    id: string;

    @Column(DataType.UUIDV4)
    @ForeignKey(() => Article)
    articleId: string;

    @BelongsTo(() => Article)
    article: Article;

    @Column(DataType.UUIDV4)
    authorId: string;

    @Column(DataType.CHAR(1000))
    text: string;

    @Column(DataType.DATE)
    createdAt: Date;

    @AllowNull
    @Column(DataType.DATE)
    updatedAt?: Date;

    @HasMany(() => Report)
    reports: Report[];
}

export default Comment;
